<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Content Editor</name>
   <tag></tag>
   <elementGuidId>564ed1e9-563a-4888-abe6-49e2a8fa6b89</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Enter name or email'])[1]/following::div[4]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.ant-select-selection-selected-value</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>390f308b-37da-4a95-aab1-d01f19bd9ed4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>ant-select-selection-selected-value</value>
      <webElementGuid>f3c0b475-f6b6-483a-a711-7ada7bba1de9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>title</name>
      <type>Main</type>
      <value>Content Editor</value>
      <webElementGuid>88862717-ef37-4d0c-81f8-37153180a110</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Content Editor</value>
      <webElementGuid>2804efd9-440c-4cae-b6ae-e6a8cd1d5e5e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[@class=&quot;app&quot;]/div[6]/div[@class=&quot;ant-modal-root&quot;]/div[@class=&quot;ant-modal-wrap&quot;]/div[@class=&quot;ant-modal set-permissions-modal workflow-users-management&quot;]/div[@class=&quot;ant-modal-content&quot;]/div[@class=&quot;ant-modal-body&quot;]/div[@class=&quot;ant-row-flex ant-row-flex-start modal-body-content&quot;]/div[@class=&quot;ant-col right-side board-members-management ant-col-xs-24 ant-col-sm-24 ant-col-md-18 ant-col-xl-15&quot;]/div[@class=&quot;user-autocomplete&quot;]/div[1]/div[@class=&quot;ant-row-flex ant-row-flex-start search-user&quot;]/div[@class=&quot;ant-col ant-col-18&quot;]/span[@class=&quot;ant-input-group ant-input-group-compact&quot;]/div[@class=&quot;ant-select-lg permission-selection ant-select ant-select-enabled ant-select-focused&quot;]/div[@class=&quot;ant-select-selection
            ant-select-selection--single&quot;]/div[@class=&quot;ant-select-selection__rendered&quot;]/div[@class=&quot;ant-select-selection-selected-value&quot;]</value>
      <webElementGuid>c4338b7a-d105-490f-bef9-5b33d203cf61</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Enter name or email'])[1]/following::div[4]</value>
      <webElementGuid>19977e43-ef34-4d58-bfdd-8a9573b79996</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Add team members to this Workflow'])[1]/following::div[10]</value>
      <webElementGuid>8b5e09a3-f481-4b91-916d-5ce17d45e40b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Invite'])[1]/preceding::div[1]</value>
      <webElementGuid>b8b2afd7-754b-4c4f-b5a4-74b5e6ffd6e0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Self Registration Link'])[1]/preceding::div[2]</value>
      <webElementGuid>cbce8176-6255-429d-9324-c2eb1af9ef13</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//span/div[2]/div/div/div</value>
      <webElementGuid>47bed2a2-ba1f-46cd-937d-0e57379ff632</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[@title = 'Content Editor' and (text() = 'Content Editor' or . = 'Content Editor')]</value>
      <webElementGuid>65270e76-67fb-4932-831e-7e2865124424</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
