<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Login to proceed</name>
   <tag></tag>
   <elementGuidId>59e49d21-d0dc-4f7e-ae23-34c9150ea9ca</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.ant-col.ant-col-24.form-text-authentication > div > span</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='content']/form/div/div[6]/div/span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>7dca3476-b697-4482-8c01-85e05d3afa70</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Login to proceed</value>
      <webElementGuid>5a95ed07-bd05-4378-8cde-b5601b7ddf72</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;content&quot;)/form[@class=&quot;ant-form ant-form-horizontal&quot;]/div[@class=&quot;ant-row-flex&quot;]/div[@class=&quot;ant-col ant-col-24 form-text-authentication&quot;]/div[1]/span[1]</value>
      <webElementGuid>4c5e1b00-b7db-4396-b2cd-b2d9c2abc82c</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='content']/form/div/div[6]/div/span</value>
      <webElementGuid>724e43d0-c84e-49f4-ad16-7f95a6e566a2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Forgot Password?'])[1]/following::span[1]</value>
      <webElementGuid>e68fa8d4-eb79-4dbc-bcd2-452b398a320c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Password'])[1]/following::span[4]</value>
      <webElementGuid>60e66f6d-9248-464a-a525-20bd37b31c6f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='.'])[1]/preceding::span[1]</value>
      <webElementGuid>c00289a3-f38d-41b4-bc9c-747846ac99db</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Your Details'])[1]/preceding::span[6]</value>
      <webElementGuid>11665f11-13b4-460b-9e4a-350debc6f324</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Login to proceed']/parent::*</value>
      <webElementGuid>7c941ea1-199f-4164-8062-a837b45c6df4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[6]/div/span</value>
      <webElementGuid>a45f532d-ac39-4c1f-be82-60796d2e98d4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'Login to proceed' or . = 'Login to proceed')]</value>
      <webElementGuid>fafa2c69-99a7-4d7d-9ed3-cd40bb108cc1</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
