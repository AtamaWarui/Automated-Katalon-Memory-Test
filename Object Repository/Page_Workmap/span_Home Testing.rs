<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Home Testing</name>
   <tag></tag>
   <elementGuidId>20285b1b-79b1-4b0b-a8db-8f51880c99ff</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>li.ant-menu-submenu.ant-menu-submenu-inline.ant-menu-submenu-active > div.ant-menu-submenu-title > span</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='root']/section/div/div[2]/div/div/div/ul/li[2]/div/span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>f0ecb419-62ca-4dd4-b10d-0ee02905b9f3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Home Testing</value>
      <webElementGuid>2d4ec5cd-bfdf-4682-9806-4a3bec690884</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/section[@class=&quot;first-layout ant-layout ant-layout-has-sider&quot;]/div[@class=&quot;drawer-content open&quot;]/div[@class=&quot;rcs-custom-scroll&quot;]/div[@class=&quot;rcs-outer-container&quot;]/div[@class=&quot;rcs-inner-container&quot;]/div[1]/ul[@class=&quot;ant-menu drawer-menu ant-menu-light ant-menu-root ant-menu-inline&quot;]/li[@class=&quot;ant-menu-submenu ant-menu-submenu-inline ant-menu-submenu-active&quot;]/div[@class=&quot;ant-menu-submenu-title&quot;]/span[1]</value>
      <webElementGuid>6ac65d7a-1017-4bf5-8ece-a12819320f29</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/section/div/div[2]/div/div/div/ul/li[2]/div/span</value>
      <webElementGuid>9424354e-6072-42f5-bf23-9210bfa636ed</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='New'])[1]/following::span[1]</value>
      <webElementGuid>b9de377a-b603-4e95-ad0d-8ff36f9bf47f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='HP'])[1]/following::span[1]</value>
      <webElementGuid>ce5ea894-5044-49d3-b277-6407659d5d90</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='New App'])[1]/preceding::span[1]</value>
      <webElementGuid>223cb454-4351-4c70-8123-f5e945c0f94e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Video Demos'])[1]/preceding::span[2]</value>
      <webElementGuid>ef6dc5e4-c3af-45b8-ac6c-2a879ba4f0eb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Home Testing']/parent::*</value>
      <webElementGuid>4948079a-be13-403a-821a-a749619b4075</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[2]/div/span</value>
      <webElementGuid>24598b75-1fca-4b7c-bc2d-4391d786e138</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'Home Testing' or . = 'Home Testing')]</value>
      <webElementGuid>64ea6bfb-603d-41ad-a085-6909cc3d770f</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
