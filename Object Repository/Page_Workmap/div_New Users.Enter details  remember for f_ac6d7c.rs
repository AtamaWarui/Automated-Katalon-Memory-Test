<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_New Users.Enter details  remember for f_ac6d7c</name>
   <tag></tag>
   <elementGuidId>c251c851-6764-4fa9-a3c7-573ede837581</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.ant-row-flex</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='content']/form/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>8e186c3d-f9d0-4669-b8ac-48a956933590</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>span</name>
      <type>Main</type>
      <value>24</value>
      <webElementGuid>65243688-d9df-4155-8631-50c009253375</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>ant-row-flex</value>
      <webElementGuid>37a27967-b44d-4523-89b8-946c8b890467</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
    
New Users.Enter details &amp; remember for future access.

        

Returning Users.Enter your credentials.


.form-view-box-wrapper .builder-form-title, .form-wrapper-box .builder-form-title {
    text-transform: capitalize;
    padding: 5px 15px;
    text-overflow: ellipsis;
    overflow: hidden;
    font-size: 25px;
    font-weight: 500;
    margin-bottom: 15px;
    margin-top: 0;
    color: #68665a;
    text-align:center;
}
.ant-form-item-label label img {
    margin-right: 10px;
    height: 18px;
    display: none;
}
.form-wrapper .ant-form-item-label label {
    color: #444;
}
.form-submit-authentication .form-submit-authentication-button {
    display: flex;
    align-items: center;
    color: #fff;
    height: 50px;
    font-size: 22px;
    background-color: #696857;
    padding: 0 15px;
    border: none;
    border-radius: 5px;
}
@media (max-width: 575px) {
.ant-form-item-control-wrapper, .ant-form-item-label {
    display: inline-block;
}}
.ant-form-item-control {
    position: relative;
    line-height: 30px;
    zoom: 1;
}
.form-builder-wrapper-view {
    height: 100%;
    overflow-x: hidden;
}
.form-forgot-password {
    color: #b5b5b5;
    margin-left: auto;
    padding: 0 17px 30px;
    margin-top: -5px;
    font-size: 13px;
    text-align: right;
    font-weight: 400;
}

Email  Password test123 Forgot Password? You may now proceedI consent to the sharing of my covid 19 Rapid test result with the test manufacturer, supplier, and their associated entities and those individuals and companies I nominate within my profile.

I Consent.  
            
Your DetailsFull Name  Birth Date May2000SuMoTuWeThFrSa301234567891011121314151617181920212223242526272829303112345678910Today Select your Province Auckland Your MyCovidRecord Email  Pick an identification document Passport Enter Document Number  Are you Fully Vaccinated against COVID19? No Your Photo Click or drag file here to upload 
        
Time to Test!
Click &quot;Add New&quot; to record a new test. Also access your test history here.
Tests Select or type here...mmj761776@gmail.com Add New Click on Submit once you are done recording your test results</value>
      <webElementGuid>18a73518-2c44-4679-a53e-4e657f3f97ea</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;content&quot;)/form[@class=&quot;ant-form ant-form-horizontal&quot;]/div[@class=&quot;ant-row-flex&quot;]</value>
      <webElementGuid>64691352-2e0f-4800-a1ad-34ec7dcc1183</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='content']/form/div</value>
      <webElementGuid>98d33824-9b62-4cab-a892-c86c669ce9ae</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Rapid Test Registration'])[1]/following::div[2]</value>
      <webElementGuid>f30dafb4-321d-4e7b-95cf-c7958665dcb8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Download'])[1]/following::div[3]</value>
      <webElementGuid>3414487c-3230-48e2-a437-08e376bbff75</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//form/div</value>
      <webElementGuid>0210a7d3-bdac-4853-aad3-bda0ca4654da</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
    
New Users.Enter details &amp; remember for future access.

        

Returning Users.Enter your credentials.


.form-view-box-wrapper .builder-form-title, .form-wrapper-box .builder-form-title {
    text-transform: capitalize;
    padding: 5px 15px;
    text-overflow: ellipsis;
    overflow: hidden;
    font-size: 25px;
    font-weight: 500;
    margin-bottom: 15px;
    margin-top: 0;
    color: #68665a;
    text-align:center;
}
.ant-form-item-label label img {
    margin-right: 10px;
    height: 18px;
    display: none;
}
.form-wrapper .ant-form-item-label label {
    color: #444;
}
.form-submit-authentication .form-submit-authentication-button {
    display: flex;
    align-items: center;
    color: #fff;
    height: 50px;
    font-size: 22px;
    background-color: #696857;
    padding: 0 15px;
    border: none;
    border-radius: 5px;
}
@media (max-width: 575px) {
.ant-form-item-control-wrapper, .ant-form-item-label {
    display: inline-block;
}}
.ant-form-item-control {
    position: relative;
    line-height: 30px;
    zoom: 1;
}
.form-builder-wrapper-view {
    height: 100%;
    overflow-x: hidden;
}
.form-forgot-password {
    color: #b5b5b5;
    margin-left: auto;
    padding: 0 17px 30px;
    margin-top: -5px;
    font-size: 13px;
    text-align: right;
    font-weight: 400;
}

Email  Password test123 Forgot Password? You may now proceedI consent to the sharing of my covid 19 Rapid test result with the test manufacturer, supplier, and their associated entities and those individuals and companies I nominate within my profile.

I Consent.  
            
Your DetailsFull Name  Birth Date May2000SuMoTuWeThFrSa301234567891011121314151617181920212223242526272829303112345678910Today Select your Province Auckland Your MyCovidRecord Email  Pick an identification document Passport Enter Document Number  Are you Fully Vaccinated against COVID19? No Your Photo Click or drag file here to upload 
        
Time to Test!
Click &quot;Add New&quot; to record a new test. Also access your test history here.
Tests Select or type here...mmj761776@gmail.com Add New Click on Submit once you are done recording your test results' or . = '
    
New Users.Enter details &amp; remember for future access.

        

Returning Users.Enter your credentials.


.form-view-box-wrapper .builder-form-title, .form-wrapper-box .builder-form-title {
    text-transform: capitalize;
    padding: 5px 15px;
    text-overflow: ellipsis;
    overflow: hidden;
    font-size: 25px;
    font-weight: 500;
    margin-bottom: 15px;
    margin-top: 0;
    color: #68665a;
    text-align:center;
}
.ant-form-item-label label img {
    margin-right: 10px;
    height: 18px;
    display: none;
}
.form-wrapper .ant-form-item-label label {
    color: #444;
}
.form-submit-authentication .form-submit-authentication-button {
    display: flex;
    align-items: center;
    color: #fff;
    height: 50px;
    font-size: 22px;
    background-color: #696857;
    padding: 0 15px;
    border: none;
    border-radius: 5px;
}
@media (max-width: 575px) {
.ant-form-item-control-wrapper, .ant-form-item-label {
    display: inline-block;
}}
.ant-form-item-control {
    position: relative;
    line-height: 30px;
    zoom: 1;
}
.form-builder-wrapper-view {
    height: 100%;
    overflow-x: hidden;
}
.form-forgot-password {
    color: #b5b5b5;
    margin-left: auto;
    padding: 0 17px 30px;
    margin-top: -5px;
    font-size: 13px;
    text-align: right;
    font-weight: 400;
}

Email  Password test123 Forgot Password? You may now proceedI consent to the sharing of my covid 19 Rapid test result with the test manufacturer, supplier, and their associated entities and those individuals and companies I nominate within my profile.

I Consent.  
            
Your DetailsFull Name  Birth Date May2000SuMoTuWeThFrSa301234567891011121314151617181920212223242526272829303112345678910Today Select your Province Auckland Your MyCovidRecord Email  Pick an identification document Passport Enter Document Number  Are you Fully Vaccinated against COVID19? No Your Photo Click or drag file here to upload 
        
Time to Test!
Click &quot;Add New&quot; to record a new test. Also access your test history here.
Tests Select or type here...mmj761776@gmail.com Add New Click on Submit once you are done recording your test results')]</value>
      <webElementGuid>cafc3bb3-99cf-4bef-b5cd-12ca7b855ffc</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
