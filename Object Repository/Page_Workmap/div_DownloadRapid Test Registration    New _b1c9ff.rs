<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_DownloadRapid Test Registration    New _b1c9ff</name>
   <tag></tag>
   <elementGuidId>a7b27515-ecee-4cdd-af45-66a85a2daded</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.form-wrapper-box</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='root']/div/div/div[2]/div/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>5294c55d-edba-49e0-ad57-48ab15cb96b4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-wrapper-box</value>
      <webElementGuid>e799441e-f612-44d4-9504-2fedc9966266</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>DownloadRapid Test Registration
    
New Users.Enter details &amp; remember for future access.

        

Returning Users.Enter your credentials.


.form-view-box-wrapper .builder-form-title, .form-wrapper-box .builder-form-title {
    text-transform: capitalize;
    padding: 5px 15px;
    text-overflow: ellipsis;
    overflow: hidden;
    font-size: 25px;
    font-weight: 500;
    margin-bottom: 15px;
    margin-top: 0;
    color: #68665a;
    text-align:center;
}
.ant-form-item-label label img {
    margin-right: 10px;
    height: 18px;
    display: none;
}
.form-wrapper .ant-form-item-label label {
    color: #444;
}
.form-submit-authentication .form-submit-authentication-button {
    display: flex;
    align-items: center;
    color: #fff;
    height: 50px;
    font-size: 22px;
    background-color: #696857;
    padding: 0 15px;
    border: none;
    border-radius: 5px;
}
@media (max-width: 575px) {
.ant-form-item-control-wrapper, .ant-form-item-label {
    display: inline-block;
}}
.ant-form-item-control {
    position: relative;
    line-height: 30px;
    zoom: 1;
}
.form-builder-wrapper-view {
    height: 100%;
    overflow-x: hidden;
}
.form-forgot-password {
    color: #b5b5b5;
    margin-left: auto;
    padding: 0 17px 30px;
    margin-top: -5px;
    font-size: 13px;
    text-align: right;
    font-weight: 400;
}

Email  Password test123 Forgot Password? You may now proceedI consent to the sharing of my covid 19 Rapid test result with the test manufacturer, supplier, and their associated entities and those individuals and companies I nominate within my profile.

I Consent.  
            
Your DetailsFull Name  Birth Date Jun2000SuMoTuWeThFrSa2829303112345678910111213141516171819202122232425262728293012345678Today Select your Province Select... Your MyCovidRecord Email  Pick an identification document Select... Enter Document Number  Are you Fully Vaccinated against COVID19? Select... Your Photo Click or drag file here to upload 
        
Time to Test!
Click &quot;Add New&quot; to record a new test. Also access your test history here.
Tests Add New Click on Submit once you are done recording your test resultsSubmitPowered by WorkMap.ai</value>
      <webElementGuid>b7656c70-6d45-4c02-ba00-cee18df1d340</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[@class=&quot;form-builder-wrapper-view form-view-box-wrapper&quot;]/div[@class=&quot;form-builder&quot;]/div[@class=&quot;ant-row&quot;]/div[@class=&quot;ant-col ant-col-xs-22 ant-col-xs-offset-1 ant-col-sm-20 ant-col-sm-offset-2 ant-col-md-18 ant-col-md-offset-3 ant-col-xl-16 ant-col-xl-offset-4&quot;]/div[@class=&quot;form-wrapper-box&quot;]</value>
      <webElementGuid>48f14a2e-d90e-4c28-af07-01d4341e9b3d</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div/div/div[2]/div/div</value>
      <webElementGuid>245f851c-6888-499b-aa47-8fb7000e1b89</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div</value>
      <webElementGuid>1b00f5fc-e22d-4bf5-8932-24e59d56505a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = 'DownloadRapid Test Registration
    
New Users.Enter details &amp; remember for future access.

        

Returning Users.Enter your credentials.


.form-view-box-wrapper .builder-form-title, .form-wrapper-box .builder-form-title {
    text-transform: capitalize;
    padding: 5px 15px;
    text-overflow: ellipsis;
    overflow: hidden;
    font-size: 25px;
    font-weight: 500;
    margin-bottom: 15px;
    margin-top: 0;
    color: #68665a;
    text-align:center;
}
.ant-form-item-label label img {
    margin-right: 10px;
    height: 18px;
    display: none;
}
.form-wrapper .ant-form-item-label label {
    color: #444;
}
.form-submit-authentication .form-submit-authentication-button {
    display: flex;
    align-items: center;
    color: #fff;
    height: 50px;
    font-size: 22px;
    background-color: #696857;
    padding: 0 15px;
    border: none;
    border-radius: 5px;
}
@media (max-width: 575px) {
.ant-form-item-control-wrapper, .ant-form-item-label {
    display: inline-block;
}}
.ant-form-item-control {
    position: relative;
    line-height: 30px;
    zoom: 1;
}
.form-builder-wrapper-view {
    height: 100%;
    overflow-x: hidden;
}
.form-forgot-password {
    color: #b5b5b5;
    margin-left: auto;
    padding: 0 17px 30px;
    margin-top: -5px;
    font-size: 13px;
    text-align: right;
    font-weight: 400;
}

Email  Password test123 Forgot Password? You may now proceedI consent to the sharing of my covid 19 Rapid test result with the test manufacturer, supplier, and their associated entities and those individuals and companies I nominate within my profile.

I Consent.  
            
Your DetailsFull Name  Birth Date Jun2000SuMoTuWeThFrSa2829303112345678910111213141516171819202122232425262728293012345678Today Select your Province Select... Your MyCovidRecord Email  Pick an identification document Select... Enter Document Number  Are you Fully Vaccinated against COVID19? Select... Your Photo Click or drag file here to upload 
        
Time to Test!
Click &quot;Add New&quot; to record a new test. Also access your test history here.
Tests Add New Click on Submit once you are done recording your test resultsSubmitPowered by WorkMap.ai' or . = 'DownloadRapid Test Registration
    
New Users.Enter details &amp; remember for future access.

        

Returning Users.Enter your credentials.


.form-view-box-wrapper .builder-form-title, .form-wrapper-box .builder-form-title {
    text-transform: capitalize;
    padding: 5px 15px;
    text-overflow: ellipsis;
    overflow: hidden;
    font-size: 25px;
    font-weight: 500;
    margin-bottom: 15px;
    margin-top: 0;
    color: #68665a;
    text-align:center;
}
.ant-form-item-label label img {
    margin-right: 10px;
    height: 18px;
    display: none;
}
.form-wrapper .ant-form-item-label label {
    color: #444;
}
.form-submit-authentication .form-submit-authentication-button {
    display: flex;
    align-items: center;
    color: #fff;
    height: 50px;
    font-size: 22px;
    background-color: #696857;
    padding: 0 15px;
    border: none;
    border-radius: 5px;
}
@media (max-width: 575px) {
.ant-form-item-control-wrapper, .ant-form-item-label {
    display: inline-block;
}}
.ant-form-item-control {
    position: relative;
    line-height: 30px;
    zoom: 1;
}
.form-builder-wrapper-view {
    height: 100%;
    overflow-x: hidden;
}
.form-forgot-password {
    color: #b5b5b5;
    margin-left: auto;
    padding: 0 17px 30px;
    margin-top: -5px;
    font-size: 13px;
    text-align: right;
    font-weight: 400;
}

Email  Password test123 Forgot Password? You may now proceedI consent to the sharing of my covid 19 Rapid test result with the test manufacturer, supplier, and their associated entities and those individuals and companies I nominate within my profile.

I Consent.  
            
Your DetailsFull Name  Birth Date Jun2000SuMoTuWeThFrSa2829303112345678910111213141516171819202122232425262728293012345678Today Select your Province Select... Your MyCovidRecord Email  Pick an identification document Select... Enter Document Number  Are you Fully Vaccinated against COVID19? Select... Your Photo Click or drag file here to upload 
        
Time to Test!
Click &quot;Add New&quot; to record a new test. Also access your test history here.
Tests Add New Click on Submit once you are done recording your test resultsSubmitPowered by WorkMap.ai')]</value>
      <webElementGuid>85cdc56d-a82a-4f8b-9a00-65eb17edf99f</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
