<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_All Tested</name>
   <tag></tag>
   <elementGuidId>1322319f-dbdd-4773-a409-9c363e4e888f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>li.ant-dropdown-menu-item.report-item.ant-dropdown-menu-item-active > span.report-name</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//section[@id='main-content-wrapper']/main/div[4]/div/div/ul/li[2]/span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>4dabdb4e-8b2c-4be0-97d1-71c2af5ea09f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>report-name</value>
      <webElementGuid>37c4856f-fdc1-4e2b-9c22-15bc18d09480</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>All Tested</value>
      <webElementGuid>2d0a17eb-21e0-418e-81bb-0652d0e5ba5d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;main-content-wrapper&quot;)/main[@class=&quot;ant-layout-content&quot;]/div[4]/div[1]/div[@class=&quot;ant-dropdown reports-list-drop-down ant-dropdown-placement-bottomLeft&quot;]/ul[@class=&quot;ant-dropdown-menu ant-dropdown-menu-light ant-dropdown-menu-root ant-dropdown-menu-vertical&quot;]/li[@class=&quot;ant-dropdown-menu-item report-item ant-dropdown-menu-item-active&quot;]/span[@class=&quot;report-name&quot;]</value>
      <webElementGuid>6d5e8413-6365-4760-bf63-f697f6b4dc39</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//section[@id='main-content-wrapper']/main/div[4]/div/div/ul/li[2]/span</value>
      <webElementGuid>0ec0d017-22e6-4b52-bbb4-06479e874abb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='All Tested Positive'])[2]/following::span[3]</value>
      <webElementGuid>c3a5e6b5-8067-4a90-a321-73910116b947</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='of'])[2]/following::span[7]</value>
      <webElementGuid>7b03d0be-7b6f-439b-b5c2-4301abcf4a1f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Marketing'])[5]/preceding::span[3]</value>
      <webElementGuid>560b53e5-7489-48ca-97ed-b2309252c492</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Production'])[2]/preceding::span[6]</value>
      <webElementGuid>e1a3e9bf-a7b8-4f3a-a98c-ccbb1822006c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='All Tested']/parent::*</value>
      <webElementGuid>128ded83-19d9-48ac-bf06-a0e713f39db1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[2]/span</value>
      <webElementGuid>9b4def39-ab38-4fdf-81bf-2a9ad9026cba</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'All Tested' or . = 'All Tested')]</value>
      <webElementGuid>bfa2516b-9ca0-4900-a00c-e2b0b1edf021</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
