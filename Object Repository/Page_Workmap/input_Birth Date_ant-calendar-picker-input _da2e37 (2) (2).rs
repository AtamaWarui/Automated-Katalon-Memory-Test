<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Birth Date_ant-calendar-picker-input _da2e37 (2) (2)</name>
   <tag></tag>
   <elementGuidId>6520940e-79c4-46f6-9d0b-5280472e76d3</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>input.ant-calendar-picker-input.ant-input</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//*[@id=&quot;content&quot;]/form/div/div[14]/div/div[2]/div/span/span/div/div/div/div/div/div/div[1]/div/input</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>61401a34-23f4-4699-a46a-08651d390a5d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>placeholder</name>
      <type>Main</type>
      <value>DD-MM-YYYY</value>
      <webElementGuid>ffb520b6-8d99-4dfa-9c37-9586d5fee48f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>ant-calendar-picker-input ant-input</value>
      <webElementGuid>c2b08a79-f3b6-4fed-9582-06698d08af24</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>value</name>
      <type>Main</type>
      <value>11-05-2022</value>
      <webElementGuid>70abadad-23ec-44c6-9fb8-0efbea2fc8ba</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>inputmode</name>
      <type>Main</type>
      <value>none</value>
      <webElementGuid>2d59033c-f4e1-4b63-ba1c-f9a87376cb9b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;content&quot;)/form[@class=&quot;ant-form ant-form-horizontal&quot;]/div[@class=&quot;ant-row-flex&quot;]/div[@class=&quot;ant-col ant-col-xs-24 ant-col-sm-24 ant-col-md-24 ant-col-lg-12 ant-col-xl-12 ant-col-xxl-12&quot;]/div[@class=&quot;ant-row ant-form-item date_input form-view-property&quot;]/div[@class=&quot;ant-col ant-form-item-control-wrapper&quot;]/div[@class=&quot;ant-form-item-control has-success&quot;]/span[@class=&quot;ant-form-item-children&quot;]/span[@class=&quot;datepicker-input ant-calendar-picker&quot;]/div[1]/input[@class=&quot;ant-calendar-picker-input ant-input&quot;]</value>
      <webElementGuid>19030052-8e37-4aeb-adba-e3aa99c90488</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@value='11-05-2022']</value>
      <webElementGuid>6f25cdac-4b3f-44c9-b86d-2ddf97e6d116</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='content']/form/div/div[14]/div/div[2]/div/span/span/div/input</value>
      <webElementGuid>bf173601-7d59-4e7c-8bae-d5df839134eb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/input</value>
      <webElementGuid>fd6d1bf9-b528-4072-9000-c8b3e82a8060</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@placeholder = 'DD-MM-YYYY']</value>
      <webElementGuid>4c309834-eb32-493f-bd95-45a93d2a049b</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
