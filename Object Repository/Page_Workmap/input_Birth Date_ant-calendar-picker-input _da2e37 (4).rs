<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Birth Date_ant-calendar-picker-input _da2e37 (4)</name>
   <tag></tag>
   <elementGuidId>997d6552-0561-4e06-b1ae-589dc226ea65</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>input.ant-calendar-picker-input.ant-input</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@value='']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>c4925cd5-9965-477e-b808-218a3b180069</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>placeholder</name>
      <type>Main</type>
      <value>DD-MM-YYYY</value>
      <webElementGuid>eee2f0e7-453d-41fb-a353-7a1488943123</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>ant-calendar-picker-input ant-input</value>
      <webElementGuid>0f7e11b3-eac3-4f49-ad25-17ff28d3e6ec</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>inputmode</name>
      <type>Main</type>
      <value>none</value>
      <webElementGuid>8cf03c7a-5bdd-4dc7-8922-5aada69780ae</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;content&quot;)/form[@class=&quot;ant-form ant-form-horizontal&quot;]/div[@class=&quot;ant-row-flex&quot;]/div[@class=&quot;ant-col ant-col-xs-24 ant-col-sm-24 ant-col-md-24 ant-col-lg-12 ant-col-xl-12 ant-col-xxl-12&quot;]/div[@class=&quot;ant-row ant-form-item date_input form-view-property&quot;]/div[@class=&quot;ant-col ant-form-item-control-wrapper&quot;]/div[@class=&quot;ant-form-item-control&quot;]/span[@class=&quot;ant-form-item-children&quot;]/span[@class=&quot;datepicker-input ant-calendar-picker&quot;]/div[1]/input[@class=&quot;ant-calendar-picker-input ant-input&quot;]</value>
      <webElementGuid>52f8559e-a7bd-492d-bcbf-7dd240d61f2d</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@value='']</value>
      <webElementGuid>ee7dc0bd-d012-4b46-9efa-a34a5c7837e7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='content']/form/div/div[4]/div/div[2]/div/span/span/div/input</value>
      <webElementGuid>b7cdece3-d395-4c66-8d82-629cb42e8ca9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/input</value>
      <webElementGuid>0e9d15df-fec5-49a7-b83d-aa5a6cac4115</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@placeholder = 'DD-MM-YYYY']</value>
      <webElementGuid>2fe3b4bb-bffb-4ba6-bb9b-0190a0467f82</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
