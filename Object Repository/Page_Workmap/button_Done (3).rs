<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Done (3)</name>
   <tag></tag>
   <elementGuidId>5974e78c-e0db-46da-86fb-8f4b5135f087</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>button.ant-btn.submit-modal.ant-btn-primary.ant-btn-lg</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//*[@id=&quot;rcDialogTitle8&quot;]/div/div[2]/button[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>656d36e4-0aec-4cb7-b200-184e9d5746d4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>f34f0b05-cd78-47d1-b18f-ca6307e44dbf</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>ant-btn submit-modal ant-btn-primary ant-btn-lg</value>
      <webElementGuid>afe7c374-7a5d-4be9-8eb6-18c0d78c8f4e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value> Done </value>
      <webElementGuid>6f374654-fb3c-434c-acaa-b1662c5bb999</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;rcDialogTitle0&quot;)/div[@class=&quot;entry-modal-header&quot;]/div[@class=&quot;entry-modal-header-right-side&quot;]/button[@class=&quot;ant-btn submit-modal ant-btn-primary ant-btn-lg&quot;]</value>
      <webElementGuid>07205693-c40d-4617-a817-a5dddd6a0591</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>(//button[@type='button'])[2]</value>
      <webElementGuid>2c04fac5-426f-4989-af5e-05754b0355a2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='rcDialogTitle0']/div/div[2]/button</value>
      <webElementGuid>f10f43ae-672d-41d2-a3ce-20e4d0758bbb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='New'])[1]/following::button[1]</value>
      <webElementGuid>2f5afb36-0306-4d0a-b665-eb4ef5178814</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Powered by WorkMap.ai'])[1]/following::button[1]</value>
      <webElementGuid>b63cb93a-74ba-4422-82d2-5d6503bf2896</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Cancel'])[1]/preceding::button[1]</value>
      <webElementGuid>22c89598-9fe7-45dd-b08d-851f0eb80d75</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/button</value>
      <webElementGuid>1614e2c1-30b7-4710-92a2-778399ca3374</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[@type = 'button' and (text() = ' Done ' or . = ' Done ')]</value>
      <webElementGuid>ff701329-23bf-4b4e-9399-5e2f0040f0a6</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
