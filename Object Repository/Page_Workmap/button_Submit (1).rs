<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Submit (1)</name>
   <tag></tag>
   <elementGuidId>5804dea7-4ce6-4e3f-83db-3f3b1d45894f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>button.ant-btn.form-builder-submit.ant-btn-primary</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//button[@type='submit']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>9daddba2-aeb4-4035-bf75-fa3bf004ca13</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>submit</value>
      <webElementGuid>fdc7c4e8-6cc2-4da3-b02d-e30316eff98e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>ant-btn form-builder-submit ant-btn-primary</value>
      <webElementGuid>7b018d0d-ac35-4876-8c3a-d911a5781c8c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Submit</value>
      <webElementGuid>8a4c37b1-b898-45e8-b3ea-03a0b4524005</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;content&quot;)/button[@class=&quot;ant-btn form-builder-submit ant-btn-primary&quot;]</value>
      <webElementGuid>cfa8bb21-d927-4555-aeaa-bd801a5ee570</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//button[@type='submit']</value>
      <webElementGuid>14f36f88-02c2-4b6d-a7a2-d69f3e0cc437</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='content']/button</value>
      <webElementGuid>91af8517-57f5-4732-99f5-63afee6fe632</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Add New'])[1]/following::button[1]</value>
      <webElementGuid>29a33f69-f33d-4e75-a304-5fa183afd91d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='mmj761776+Test2@gmail.com'])[1]/following::button[1]</value>
      <webElementGuid>af9aff38-6717-40e4-ab1a-c865e36242aa</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Powered by WorkMap.ai'])[1]/preceding::button[1]</value>
      <webElementGuid>69d42bcb-1828-4d15-b699-6ee30cc8dda5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/button</value>
      <webElementGuid>968d5eb7-65b7-48fc-9d17-8264ec7312cc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[@type = 'submit' and (text() = 'Submit' or . = 'Submit')]</value>
      <webElementGuid>15346756-66f4-4a23-baf7-c93e5bd0f1ce</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
