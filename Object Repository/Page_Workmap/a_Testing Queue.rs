<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Testing Queue</name>
   <tag></tag>
   <elementGuidId>ab65df03-75a5-4e88-83db-c71f015e8f7a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>li.ant-menu-item.ant-menu-item-active > a</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//ul[@id='4$Menu']/li[2]/a</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>4bae9b3a-7914-49b2-946e-0e6ead1f906d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>#/boards/7</value>
      <webElementGuid>d01fae00-dd08-46a7-bf2b-713542d1c0cf</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Testing Queue</value>
      <webElementGuid>6eb56beb-5f84-4874-ad2b-1a6f06d6e450</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;4$Menu&quot;)/li[@class=&quot;ant-menu-item ant-menu-item-active&quot;]/a[1]</value>
      <webElementGuid>d1a432c6-c4e0-4216-ae16-ac61cfbb78fe</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//ul[@id='4$Menu']/li[2]/a</value>
      <webElementGuid>6445de89-62cc-43f0-900f-4a265bb2ab72</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'Testing Queue')]</value>
      <webElementGuid>f2cb0cf2-f94f-498d-a15f-bd0835019fc4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Dashboard'])[1]/following::a[1]</value>
      <webElementGuid>c6210343-c834-4cb5-927e-419cf8b29baa</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='PoC Testing'])[1]/following::a[2]</value>
      <webElementGuid>de09cace-2d0f-49b4-8ec9-f8edf789fe23</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='All Patients'])[1]/preceding::a[1]</value>
      <webElementGuid>dc41fcd8-c3a9-4a1a-8176-f77dfb13f30b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='HP'])[1]/preceding::a[2]</value>
      <webElementGuid>71eb609d-a7df-4ccc-a237-b2bd8aee4663</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Testing Queue']/parent::*</value>
      <webElementGuid>38308fef-bddd-4124-ba3e-88f6902b3829</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, '#/boards/7')]</value>
      <webElementGuid>a44a9bf1-e9da-4130-9f22-bbb21c7f9ef5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[2]/a</value>
      <webElementGuid>762c3287-e79d-4d6b-a803-cefb06363dbe</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = '#/boards/7' and (text() = 'Testing Queue' or . = 'Testing Queue')]</value>
      <webElementGuid>44070c2c-ae50-451f-9fb3-93aba8ea04be</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
