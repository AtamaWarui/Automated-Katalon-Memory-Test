<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_All Patients</name>
   <tag></tag>
   <elementGuidId>4faf2331-096a-4495-8ede-2397501e23ee</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>li.ant-menu-item.ant-menu-item-active > a</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//ul[@id='4$Menu']/li[3]/a</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>2c6d6c63-4704-49ee-9c93-2816cd1381ab</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>#/boards/8</value>
      <webElementGuid>fe681fc7-59b2-49dc-97d4-7b837d3ae8f7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>All Patients</value>
      <webElementGuid>6abc87bc-f3a3-4843-a350-7ff6ae34d5bb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;4$Menu&quot;)/li[@class=&quot;ant-menu-item ant-menu-item-active&quot;]/a[1]</value>
      <webElementGuid>4b7172da-e9db-4794-8e20-96ec9a7ab25c</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//ul[@id='4$Menu']/li[3]/a</value>
      <webElementGuid>850272be-9d76-4354-8633-d1b75a0f6b37</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'All Patients')]</value>
      <webElementGuid>a0f44eba-e2cc-4028-871c-888c2bdbca1c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Testing Queue'])[1]/following::a[1]</value>
      <webElementGuid>035a736c-aab9-437c-8431-9476c738f8f1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Dashboard'])[1]/following::a[2]</value>
      <webElementGuid>200c589d-450d-4a0b-8bb0-56718783f633</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='HP'])[1]/preceding::a[1]</value>
      <webElementGuid>27621fb4-d027-46a8-94a0-9e77a7011f7f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='New'])[1]/preceding::a[2]</value>
      <webElementGuid>6ff27b34-53a6-4914-a7e0-c2197b20fb9b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='All Patients']/parent::*</value>
      <webElementGuid>85073d1c-6b6f-476b-901e-997b248f96ad</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, '#/boards/8')]</value>
      <webElementGuid>76d4a1d0-dae2-4f04-824d-9a178c3e1834</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[3]/a</value>
      <webElementGuid>81ee0977-6f7e-46b6-ae82-c8b8971c3b57</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = '#/boards/8' and (text() = 'All Patients' or . = 'All Patients')]</value>
      <webElementGuid>f3a821c6-a719-4fc9-896f-61e874676e03</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
