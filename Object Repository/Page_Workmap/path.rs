<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>path</name>
   <tag></tag>
   <elementGuidId>86ae4570-1c96-46e1-92f0-c4ea4a02786d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>i.anticon.anticon-caret-left > svg > path</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//*[@id=&quot;content&quot;]/form/div/div[9]/div/div[2]/div/span/div/div/div/i/svg/path</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>path</value>
      <webElementGuid>efa6d660-4d72-497e-b7f9-3c6ff1b3fb15</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>d</name>
      <type>Main</type>
      <value>M689 165.1L308.2 493.5c-10.9 9.4-10.9 27.5 0 37L689 858.9c14.2 12.2 35 1.2 35-18.5V183.6c0-19.7-20.8-30.7-35-18.5z</value>
      <webElementGuid>54552575-d239-4d13-8d93-ad363c3f082f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;content&quot;)/form[@class=&quot;ant-form ant-form-horizontal&quot;]/div[@class=&quot;ant-row-flex&quot;]/div[@class=&quot;ant-col ant-col-xs-24 ant-col-sm-24 ant-col-md-24 ant-col-lg-12 ant-col-xl-12 ant-col-xxl-12&quot;]/div[@class=&quot;ant-row ant-form-item qr_code_input form-view-property&quot;]/div[@class=&quot;ant-col ant-form-item-control-wrapper&quot;]/div[@class=&quot;ant-form-item-control has-success&quot;]/span[@class=&quot;ant-form-item-children&quot;]/div[1]/div[@class=&quot;barcode-camera&quot;]/div[@class=&quot;barcode-title&quot;]/i[@class=&quot;anticon anticon-caret-left&quot;]/svg[1]/path[1]</value>
      <webElementGuid>3d74d54b-0a12-4f2c-b2a5-f82df0fd5f27</webElementGuid>
   </webElementProperties>
</WebElementEntity>
