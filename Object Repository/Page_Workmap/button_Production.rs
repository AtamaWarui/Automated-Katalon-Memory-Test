<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Production</name>
   <tag></tag>
   <elementGuidId>80efc429-a397-406e-a2a5-5eb8f68b1510</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>button.ant-btn.report-name-drop-down.ant-dropdown-trigger.ant-btn-lg</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(//button[@type='button'])[5]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>b1f4b905-1090-494c-aa26-77caa4301dc4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>d3cfe3fd-e405-4cc3-bca2-d3a09e5d4714</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>ant-btn report-name-drop-down ant-dropdown-trigger ant-btn-lg</value>
      <webElementGuid>0daf0514-781e-42cc-814f-4f3b0dbe74f0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ant-click-animating-without-extra-node</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>d607e9f7-0592-4d70-8465-d52bf647ad1a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Production </value>
      <webElementGuid>a64de197-ef52-4843-80e0-3c1d75b16dd6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;main-content-wrapper&quot;)/main[@class=&quot;ant-layout-content&quot;]/div[@class=&quot;custom-header custom-report-header sticky-component clearfix&quot;]/ul[@class=&quot;pull-right board-custom-header-right report-header&quot;]/li[5]/button[@class=&quot;ant-btn report-name-drop-down ant-dropdown-trigger ant-btn-lg&quot;]</value>
      <webElementGuid>b90601d8-af31-47dc-9c36-2d0b0c9e4760</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>(//button[@type='button'])[5]</value>
      <webElementGuid>56707500-1280-4c32-85a8-5582f8a92a61</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//section[@id='main-content-wrapper']/main/div/ul/li[5]/button</value>
      <webElementGuid>813ab159-56ad-4ba7-93d9-d5c5e64f404f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Share'])[1]/following::button[2]</value>
      <webElementGuid>aa8604c8-58b6-425f-b754-0b7a9d716334</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='New Report'])[1]/following::button[3]</value>
      <webElementGuid>a447315e-8c9a-42c3-8346-24b400020b40</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Drag here to set row groups'])[1]/preceding::button[1]</value>
      <webElementGuid>771be135-99ff-43da-9aed-7002c93d4bd4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[5]/button</value>
      <webElementGuid>02efc74b-00ab-4091-a9ea-bec22c39ccdc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[@type = 'button' and (text() = 'Production ' or . = 'Production ')]</value>
      <webElementGuid>aae61d34-10cf-4929-8cdc-04e5616e88e6</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
