<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_New Password and Confirm Password are n_e1b960</name>
   <tag></tag>
   <elementGuidId>691a2895-3578-42a9-adbf-c5aae1b9eb3b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.ant-col.ant-col-xs-24.ant-col-sm-24.ant-col-md-12.ant-col-md-offset-12.ant-col-xl-12</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Member'])[3]/following::div[11]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>5081a3ab-913e-48d9-8141-c4d34b27707d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>ant-col ant-col-xs-24 ant-col-sm-24 ant-col-md-12 ant-col-md-offset-12 ant-col-xl-12</value>
      <webElementGuid>a76cdf65-9931-4412-b4f2-4fb001acb3d4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>New Password and Confirm Password are not the same.</value>
      <webElementGuid>881134fe-c88e-4cf6-bb9e-3099ce14b4b9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[@class=&quot;app&quot;]/div[6]/div[@class=&quot;ant-modal-root&quot;]/div[@class=&quot;ant-modal-wrap matching-modal&quot;]/div[@class=&quot;ant-modal&quot;]/div[@class=&quot;ant-modal-content&quot;]/div[@class=&quot;ant-modal-body&quot;]/form[@class=&quot;ant-form ant-form-horizontal create-portal-form&quot;]/div[@class=&quot;ant-row&quot;]/div[@class=&quot;ant-col ant-col-xs-24 ant-col-sm-24 ant-col-md-12 ant-col-md-offset-12 ant-col-xl-12&quot;]</value>
      <webElementGuid>b68a7889-455b-4862-b464-54171547048f</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Member'])[3]/following::div[11]</value>
      <webElementGuid>16a37f16-47ff-4057-9289-28208610e9d7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Account Admin'])[2]/following::div[11]</value>
      <webElementGuid>478c8a03-da3c-46a6-9cb9-f462bb4a824c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Make sure to send your users their login information - email user and password'])[1]/preceding::div[5]</value>
      <webElementGuid>564d7696-30df-470b-a4d2-d2e491dfff28</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//form/div[3]/div[3]</value>
      <webElementGuid>34b69d3b-a8f3-4011-8ce4-7e79be654bf6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = 'New Password and Confirm Password are not the same.' or . = 'New Password and Confirm Password are not the same.')]</value>
      <webElementGuid>8cce420e-e369-44b9-b21a-6776344cd7bf</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
