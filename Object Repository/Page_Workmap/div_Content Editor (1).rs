<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Content Editor (1)</name>
   <tag></tag>
   <elementGuidId>0c39130d-d248-4425-9fd3-1c1979b28059</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Enter name or email'])[1]/following::div[4]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.ant-select-selection-selected-value</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>1fde47ad-79ca-4469-b2e3-3f5bd715a220</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>ant-select-selection-selected-value</value>
      <webElementGuid>8f0daf11-b737-4dca-b32d-b7aa83dd1594</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>title</name>
      <type>Main</type>
      <value>Content Editor</value>
      <webElementGuid>5fd9e3df-ec3c-4451-80f9-cd755d373dc7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Content Editor</value>
      <webElementGuid>d9dc27ca-3112-4126-a0b1-bf22be9d9372</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[@class=&quot;app&quot;]/div[6]/div[@class=&quot;ant-modal-root&quot;]/div[@class=&quot;ant-modal-wrap&quot;]/div[@class=&quot;ant-modal set-permissions-modal workflow-users-management&quot;]/div[@class=&quot;ant-modal-content&quot;]/div[@class=&quot;ant-modal-body&quot;]/div[@class=&quot;ant-row-flex ant-row-flex-start modal-body-content&quot;]/div[@class=&quot;ant-col right-side board-members-management ant-col-xs-24 ant-col-sm-24 ant-col-md-18 ant-col-xl-15&quot;]/div[@class=&quot;user-autocomplete&quot;]/div[1]/div[@class=&quot;ant-row-flex ant-row-flex-start search-user&quot;]/div[@class=&quot;ant-col ant-col-18&quot;]/span[@class=&quot;ant-input-group ant-input-group-compact&quot;]/div[@class=&quot;ant-select-lg permission-selection ant-select ant-select-enabled ant-select-focused&quot;]/div[@class=&quot;ant-select-selection
            ant-select-selection--single&quot;]/div[@class=&quot;ant-select-selection__rendered&quot;]/div[@class=&quot;ant-select-selection-selected-value&quot;]</value>
      <webElementGuid>d302cbb1-51f1-4120-9b5d-ae8cd7b6c598</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Enter name or email'])[1]/following::div[4]</value>
      <webElementGuid>b9aaa9db-ca25-4090-a327-5ae1b2a6c04b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Add team members to this Workflow'])[1]/following::div[10]</value>
      <webElementGuid>6cfd80bb-5cb4-4473-88ff-5d6050bb64ed</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Invite'])[1]/preceding::div[1]</value>
      <webElementGuid>6a68f8ff-cd96-431b-ab5b-8d811d3a2283</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Self Registration Link'])[1]/preceding::div[2]</value>
      <webElementGuid>cdb89849-9469-48ee-8ae6-fdb0063c0285</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//span/div[2]/div/div/div</value>
      <webElementGuid>49f8737c-b856-44fd-bdb7-c2a74b6ff362</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[@title = 'Content Editor' and (text() = 'Content Editor' or . = 'Content Editor')]</value>
      <webElementGuid>4ffadc9a-9065-4052-b7f0-0f115148208f</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
