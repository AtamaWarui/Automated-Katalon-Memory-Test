<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>i__mdi mdi-pencil</name>
   <tag></tag>
   <elementGuidId>ca924616-7d52-435c-a660-729b93d8c042</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>i.mdi.mdi-pencil</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='rcDialogTitle0']/div/div[2]/a/i</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>i</value>
      <webElementGuid>e1c9b492-0b41-4335-a566-5747b9608f07</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>mdi mdi-pencil</value>
      <webElementGuid>348e9c70-1216-4283-82d8-1d3107ba3b4a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;rcDialogTitle0&quot;)/div[@class=&quot;entry-modal-header&quot;]/div[@class=&quot;entry-modal-header-right-side&quot;]/a[1]/i[@class=&quot;mdi mdi-pencil&quot;]</value>
      <webElementGuid>ddb71a25-e37f-488f-b8d8-1a7b5fb1f7f9</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='rcDialogTitle0']/div/div[2]/a/i</value>
      <webElementGuid>54726d82-4121-4d2f-abd7-e32eb5e517aa</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div/div[2]/a/i</value>
      <webElementGuid>c5b27679-4458-4d48-a9e3-ad722f66d69a</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
