<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>li_Realy Tech Nasal (2)</name>
   <tag></tag>
   <elementGuidId>5fedb1b3-07cc-48ca-bea5-67a5110c57c3</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>li.ant-dropdown-menu-item.available-option.ant-dropdown-menu-item-active</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='content']/div[3]/div/div/ul/li</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>li</value>
      <webElementGuid>f736f536-cbac-464d-9c51-488605ec2d08</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>ant-dropdown-menu-item available-option ant-dropdown-menu-item-active</value>
      <webElementGuid>c88b01ee-0a3b-4a63-b2ae-7a2d4ca79d6e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>role</name>
      <type>Main</type>
      <value>menuitem</value>
      <webElementGuid>ac95cc5e-356a-49ac-9182-31ae2b259eb1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Realy Tech Nasal</value>
      <webElementGuid>17d8b4fe-3c98-4a8c-8190-c26f8dcae4b3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[@class=&quot;app&quot;]/div[4]/div[@class=&quot;ant-modal-root&quot;]/div[@class=&quot;ant-modal-wrap entry-modal linked-item-details&quot;]/div[@class=&quot;ant-modal&quot;]/div[@class=&quot;ant-modal-content&quot;]/div[@class=&quot;ant-modal-body&quot;]/div[1]/div[@class=&quot;ant-row&quot;]/div[@class=&quot;ant-col ant-col-24 modal-content default-modal&quot;]/div[@id=&quot;content&quot;]/div[@id=&quot;content&quot;]/div[3]/div[1]/div[@class=&quot;ant-dropdown scrollable-dropdown ant-dropdown-placement-bottomLeft&quot;]/ul[@class=&quot;ant-dropdown-menu ant-dropdown-menu-light ant-dropdown-menu-root ant-dropdown-menu-vertical&quot;]/li[@class=&quot;ant-dropdown-menu-item available-option ant-dropdown-menu-item-active&quot;]</value>
      <webElementGuid>6f0a2b36-aa9a-4403-8d90-65f52d08ff9e</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='content']/div[3]/div/div/ul/li</value>
      <webElementGuid>5c7cdfc0-28e4-4485-8a79-9ad972ff9299</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='.'])[3]/following::li[1]</value>
      <webElementGuid>d177e09f-b72f-47f2-a0b6-fe49cb4c6abc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Family Member Email'])[1]/following::li[1]</value>
      <webElementGuid>13fff93d-9979-430b-8710-e937c2693e28</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='V-Chek Nasal'])[2]/preceding::li[1]</value>
      <webElementGuid>8478f8e5-dd1d-41ba-b5ec-ca82aa98deb6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Lolly Saliva'])[2]/preceding::li[2]</value>
      <webElementGuid>59006d6b-eb60-49fe-9c7e-7705aa794f09</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//ul/li</value>
      <webElementGuid>765786b4-b6f8-476b-9a1a-30f23622fa0a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//li[(text() = 'Realy Tech Nasal' or . = 'Realy Tech Nasal')]</value>
      <webElementGuid>8e4b1873-1eca-4bf2-92c1-9f795552e508</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
