<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>svg (8)</name>
   <tag></tag>
   <elementGuidId>d4c41ab1-c55f-4d9d-a458-1f07ed6100bd</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Powered by WorkMap.ai'])[1]/following::*[name()='svg'][1]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>i.anticon.anticon-close.ant-modal-close-icon > svg</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>svg</value>
      <webElementGuid>c2928a93-5fcb-4e80-9850-37ba2ba7a22b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>viewBox</name>
      <type>Main</type>
      <value>64 64 896 896</value>
      <webElementGuid>0648768f-3643-4022-bb17-6d69e3f570b7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>focusable</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>4c0390f0-f1df-4008-bb24-2211943ddcfb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-icon</name>
      <type>Main</type>
      <value>close</value>
      <webElementGuid>cfdf5d88-0aeb-4b8a-bd06-7ea0cdc570c1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>width</name>
      <type>Main</type>
      <value>1em</value>
      <webElementGuid>57ee285d-b8a9-46e9-bf0a-68225809a71e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>height</name>
      <type>Main</type>
      <value>1em</value>
      <webElementGuid>cb1930d2-a178-4fd8-9b67-bab33bc42353</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>fill</name>
      <type>Main</type>
      <value>currentColor</value>
      <webElementGuid>cb61af8a-7ae3-4cc5-a7dc-d389a328c3d8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-hidden</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>a9bba45b-4004-4d3f-9287-5f28b49aa456</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[@class=&quot;app&quot;]/div[4]/div[@class=&quot;ant-modal-root&quot;]/div[@class=&quot;ant-modal-wrap&quot;]/div[@class=&quot;ant-modal&quot;]/div[@class=&quot;ant-modal-content&quot;]/button[@class=&quot;ant-modal-close&quot;]/span[@class=&quot;ant-modal-close-x&quot;]/i[@class=&quot;anticon anticon-close ant-modal-close-icon&quot;]/svg[1]</value>
      <webElementGuid>91c6de72-2ee3-49a8-9013-7e8f9514b93e</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Powered by WorkMap.ai'])[1]/following::*[name()='svg'][1]</value>
      <webElementGuid>e99f736e-d6b1-4560-a2e6-569cd937c0fc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Submit'])[1]/following::*[name()='svg'][1]</value>
      <webElementGuid>e4b4d0ae-aa88-40b1-86c4-8ad50686733f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Share your form with others'])[1]/preceding::*[name()='svg'][1]</value>
      <webElementGuid>23e4b396-155e-4d00-8a34-4708086e9150</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Submission to this Workflow will create records in your table to kick off your workflows'])[1]/preceding::*[name()='svg'][1]</value>
      <webElementGuid>5e569cd6-33bf-49f1-ac15-d6f76aa6e7d5</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
