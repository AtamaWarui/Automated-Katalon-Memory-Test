<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Mounia testJanah</name>
   <tag></tag>
   <elementGuidId>5a3341e3-66a9-4e06-9608-364075f9c542</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>li.ant-select-dropdown-menu-item.mapping-selected-item.ant-select-dropdown-menu-item-active > div.display-value-wrapper > span.entry-name</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//*[@id=&quot;all_patients_335&quot;]/div/div/div[2]/div/span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>e6ea1455-77cb-4782-ab9b-1bcc986b78fa</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>entry-name</value>
      <webElementGuid>c5c071d1-98c0-4d11-b419-081e57b0d059</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Mounia testJanah</value>
      <webElementGuid>8846426c-7439-41e6-bd87-ea2e63c5cf4f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;b43b6e3a-0ec7-4836-b883-7ce1fcca0b3c&quot;)/ul[@class=&quot;ant-select-dropdown-menu  ant-select-dropdown-menu-root ant-select-dropdown-menu-vertical&quot;]/li[@class=&quot;ant-select-dropdown-menu-item mapping-selected-item ant-select-dropdown-menu-item-active&quot;]/div[@class=&quot;display-value-wrapper&quot;]/span[@class=&quot;entry-name&quot;]</value>
      <webElementGuid>710f0655-22cd-49bd-bd95-6ace1a68c8a9</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='b43b6e3a-0ec7-4836-b883-7ce1fcca0b3c']/ul/li[14]/div/span</value>
      <webElementGuid>1dc02125-2b20-42cd-a245-6d8dc6689bb5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Pankaj E1'])[1]/following::span[1]</value>
      <webElementGuid>649a91ee-8bed-491c-989d-e19dff5fa542</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Pankaj Taneja'])[3]/following::span[2]</value>
      <webElementGuid>650b4a7a-d271-4a9f-af61-dd7c8c1e32a0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='mmj761776@gmail.com'])[1]/preceding::span[1]</value>
      <webElementGuid>8d4a3061-81ab-4c66-82fb-18b1cdc584bc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Powered by WorkMap.ai'])[1]/preceding::span[2]</value>
      <webElementGuid>c5194850-9388-46c4-83de-1978b7a781f8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Mounia testJanah']/parent::*</value>
      <webElementGuid>f4f78df6-c4dd-4939-ad4f-25e9aa01ce7f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[14]/div/span</value>
      <webElementGuid>f158f339-684d-4dc3-a806-8e6b568b04a2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'Mounia testJanah' or . = 'Mounia testJanah')]</value>
      <webElementGuid>6336b7e5-838d-4a07-a5f8-906b9b9d93f8</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
