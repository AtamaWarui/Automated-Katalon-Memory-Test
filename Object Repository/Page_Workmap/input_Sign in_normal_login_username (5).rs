<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Sign in_normal_login_username (5)</name>
   <tag></tag>
   <elementGuidId>d8032eda-c65f-4dcf-89a5-b18fd9f831a5</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='normal_login_username']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#normal_login_username</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>a7655932-970e-4eb3-b08d-a6e8df44cd63</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>placeholder</name>
      <type>Main</type>
      <value>Email Address</value>
      <webElementGuid>30fc03ec-1d65-484f-b2d4-2c7277cdaab4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>text</value>
      <webElementGuid>d8626901-b361-4994-a004-e312dda47c20</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>normal_login_username</value>
      <webElementGuid>46a734b8-a237-4ea9-b881-bad284ed96ab</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-__meta</name>
      <type>Main</type>
      <value>[object Object]</value>
      <webElementGuid>ffc2d37c-c2bc-4fbf-ae47-929824deb1e9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-__field</name>
      <type>Main</type>
      <value>[object Object]</value>
      <webElementGuid>3c410919-8f10-4036-8cd2-83070cdc552e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>ant-input</value>
      <webElementGuid>5fb4c0d9-0066-4afc-ba14-66ea31d8b4fd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;normal_login_username&quot;)</value>
      <webElementGuid>d827f357-3a81-48ad-9215-5619ed8e4810</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='normal_login_username']</value>
      <webElementGuid>7607e802-fe0a-4e31-8c26-52319ceec8d5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div/div[2]/form/div[3]/div/div/span/span/input</value>
      <webElementGuid>18460aac-c67b-4814-94b4-ee0a2ea211d1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//input</value>
      <webElementGuid>eab7b97c-f209-4104-94aa-321dbcc40408</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@placeholder = 'Email Address' and @type = 'text' and @id = 'normal_login_username']</value>
      <webElementGuid>4e41dee5-3dcf-4a74-8ae0-cef44e0ac6c1</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
