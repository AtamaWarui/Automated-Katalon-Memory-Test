<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>li_Production</name>
   <tag></tag>
   <elementGuidId>827d575f-0641-40fa-a2f6-1de138806948</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>li.ant-dropdown-menu-item.report-item.ant-dropdown-menu-item-active</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//section[@id='main-content-wrapper']/main/div[4]/div/div/ul/li[4]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>li</value>
      <webElementGuid>53effe4d-26dd-48ca-ba71-eb1aff785f5b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>ant-dropdown-menu-item report-item ant-dropdown-menu-item-active</value>
      <webElementGuid>7ca6f01d-6ded-4544-91dc-e72668b0412c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>role</name>
      <type>Main</type>
      <value>menuitem</value>
      <webElementGuid>99cca694-9749-4ee6-87e8-88b7346afc9d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Production</value>
      <webElementGuid>96d4c6e3-4a1b-494c-a553-e1e5bca812b8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;main-content-wrapper&quot;)/main[@class=&quot;ant-layout-content&quot;]/div[4]/div[1]/div[@class=&quot;ant-dropdown reports-list-drop-down ant-dropdown-placement-bottomLeft&quot;]/ul[@class=&quot;ant-dropdown-menu ant-dropdown-menu-light ant-dropdown-menu-root ant-dropdown-menu-vertical&quot;]/li[@class=&quot;ant-dropdown-menu-item report-item ant-dropdown-menu-item-active&quot;]</value>
      <webElementGuid>a9952bb7-ee53-45e3-b8cd-3ec9701fb85e</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//section[@id='main-content-wrapper']/main/div[4]/div/div/ul/li[4]</value>
      <webElementGuid>5dd84f42-1637-4932-9212-5c79a8d0c1ae</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Marketing'])[2]/following::li[1]</value>
      <webElementGuid>fa6e8b5b-b553-40cc-93e3-d4233a645ad7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='All Tested'])[1]/following::li[2]</value>
      <webElementGuid>ecac1d47-53c7-457b-aedc-c7aec55f568c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='HR'])[1]/preceding::li[1]</value>
      <webElementGuid>c5cc91e2-832b-48da-9b88-74074421652a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div/ul/li[4]</value>
      <webElementGuid>7c022eb2-b11a-4290-af2f-6b8b35b554d6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//li[(text() = 'Production' or . = 'Production')]</value>
      <webElementGuid>154d3739-f786-4eec-aa13-7b9b584219a5</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
