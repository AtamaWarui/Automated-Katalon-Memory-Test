<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>svg_1</name>
   <tag></tag>
   <elementGuidId>5b292ce7-49d1-4e14-b6a1-ff9cb37a548a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>a.main-icon-forward-back.false > svg</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Video Demos'])[1]/following::*[name()='svg'][1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>svg</value>
      <webElementGuid>73469bde-4280-42ca-afb3-d7aeb68a52cb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>width</name>
      <type>Main</type>
      <value>20</value>
      <webElementGuid>7a7f3f0c-51b0-4b26-a200-8bcc669fe826</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>height</name>
      <type>Main</type>
      <value>20</value>
      <webElementGuid>07beca01-a18c-4da1-b085-5c9de9a9a13b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>viewBox</name>
      <type>Main</type>
      <value>0 0 24 24</value>
      <webElementGuid>e611eebb-b73b-40d6-b2c6-2eb208f64310</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;main-content-wrapper&quot;)/a[@class=&quot;main-icon-forward-back false&quot;]/svg[1]</value>
      <webElementGuid>ed26a1b1-0bf6-4086-8d9e-63bd9aa4c1aa</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Video Demos'])[1]/following::*[name()='svg'][1]</value>
      <webElementGuid>a9680677-7270-4421-af50-28604de4cfd6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='New App'])[1]/following::*[name()='svg'][1]</value>
      <webElementGuid>7a505ef7-25cf-42b2-819e-48f8b86d203c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Pages'])[1]/preceding::*[name()='svg'][1]</value>
      <webElementGuid>ad02cf30-0788-43b5-a773-2145f55fe9de</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Solasta Testing Apps'])[1]/preceding::*[name()='svg'][2]</value>
      <webElementGuid>9175482f-7040-4fda-a231-0040a45b7514</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
