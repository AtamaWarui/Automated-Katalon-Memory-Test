<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>svg (7)</name>
   <tag></tag>
   <elementGuidId>7b0e80f8-d1d6-4407-ae35-6d8e50a18ac6</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>a.main-icon-forward-back.false > svg</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Video Demos'])[1]/following::*[name()='svg'][1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>svg</value>
      <webElementGuid>caacb084-1fa9-4ce0-8413-d98d41d3b2d6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>width</name>
      <type>Main</type>
      <value>20</value>
      <webElementGuid>e6200ab2-8dd6-4451-b941-82c0e5fc1c8c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>height</name>
      <type>Main</type>
      <value>20</value>
      <webElementGuid>12630e49-6ab9-4ec6-b4fc-07efbfbd4da0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>viewBox</name>
      <type>Main</type>
      <value>0 0 24 24</value>
      <webElementGuid>ccc0f878-0cfd-4302-aab2-bc8b2f5c58a6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;main-content-wrapper&quot;)/a[@class=&quot;main-icon-forward-back false&quot;]/svg[1]</value>
      <webElementGuid>24c843f0-eaf4-4c26-9523-4626a343913b</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Video Demos'])[1]/following::*[name()='svg'][1]</value>
      <webElementGuid>8828884c-b019-4ee6-b11a-8301c8987442</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='New App'])[1]/following::*[name()='svg'][1]</value>
      <webElementGuid>6fed2838-5990-4ea1-a1b8-a50b8135acb0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Brainfood PoC Testing'])[1]/preceding::*[name()='svg'][1]</value>
      <webElementGuid>3b93d9bc-c3a4-419b-af68-0a0375c55aea</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='PoC Testing Application'])[1]/preceding::*[name()='svg'][2]</value>
      <webElementGuid>0b458388-bb8b-4388-82a9-7151ced47602</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
