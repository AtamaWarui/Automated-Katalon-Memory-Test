<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_DownloadRapid Test Registration    New _b1c9ff (1)</name>
   <tag></tag>
   <elementGuidId>03f19346-3837-4b04-aa97-7eebba7977c2</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.form-wrapper-box</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='root']/div/div/div[2]/div/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>3766968d-2668-4b41-99c9-38a4d9fe20e0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-wrapper-box</value>
      <webElementGuid>27839c18-605f-48e7-a01e-57226d4ddb0c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>DownloadRapid Test Registration
    
New Users.Enter details &amp; remember for future access.

        

Returning Users.Enter your credentials.


.form-view-box-wrapper .builder-form-title, .form-wrapper-box .builder-form-title {
    text-transform: capitalize;
    padding: 5px 15px;
    text-overflow: ellipsis;
    overflow: hidden;
    font-size: 25px;
    font-weight: 500;
    margin-bottom: 15px;
    margin-top: 0;
    color: #68665a;
    text-align:center;
}
.ant-form-item-label label img {
    margin-right: 10px;
    height: 18px;
    display: none;
}
.form-wrapper .ant-form-item-label label {
    color: #444;
}
.form-submit-authentication .form-submit-authentication-button {
    display: flex;
    align-items: center;
    color: #fff;
    height: 50px;
    font-size: 22px;
    background-color: #696857;
    padding: 0 15px;
    border: none;
    border-radius: 5px;
}
@media (max-width: 575px) {
.ant-form-item-control-wrapper, .ant-form-item-label {
    display: inline-block;
}}
.ant-form-item-control {
    position: relative;
    line-height: 30px;
    zoom: 1;
}
.form-builder-wrapper-view {
    height: 100%;
    overflow-x: hidden;
}
.form-forgot-password {
    color: #b5b5b5;
    margin-left: auto;
    padding: 0 17px 30px;
    margin-top: -5px;
    font-size: 13px;
    text-align: right;
    font-weight: 400;
}

Email  Password test123 Forgot Password? You may now proceedI consent to the sharing of my covid 19 Rapid test result with the test manufacturer, supplier, and their associated entities and those individuals and companies I nominate within my profile.

I Consent.  
            
Your DetailsFull Name  Birth Date May2000SuMoTuWeThFrSa301234567891011121314151617181920212223242526272829303112345678910Today Select your Province Select... Your MyCovidRecord Email  Pick an identification document Select... Enter Document Number  Are you Fully Vaccinated against COVID19? Select... Your Photo Click or drag file here to upload 
        
Time to Test!
Click &quot;Add New&quot; to record a new test. Also access your test history here.
Tests Add New Click on Submit once you are done recording your test resultsSubmitPowered by WorkMap.ai</value>
      <webElementGuid>368f7530-d06a-4983-8224-f021a7249233</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[@class=&quot;form-builder-wrapper-view form-view-box-wrapper&quot;]/div[@class=&quot;form-builder&quot;]/div[@class=&quot;ant-row&quot;]/div[@class=&quot;ant-col ant-col-xs-22 ant-col-xs-offset-1 ant-col-sm-20 ant-col-sm-offset-2 ant-col-md-18 ant-col-md-offset-3 ant-col-xl-16 ant-col-xl-offset-4&quot;]/div[@class=&quot;form-wrapper-box&quot;]</value>
      <webElementGuid>f6a5f62f-5b66-4c1c-a223-1e8d54349d8b</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div/div/div[2]/div/div</value>
      <webElementGuid>3c0e58a5-9fbd-4672-b67d-ac918479e739</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div</value>
      <webElementGuid>db4d2faf-8d14-43ed-8d47-f97546ed1608</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = 'DownloadRapid Test Registration
    
New Users.Enter details &amp; remember for future access.

        

Returning Users.Enter your credentials.


.form-view-box-wrapper .builder-form-title, .form-wrapper-box .builder-form-title {
    text-transform: capitalize;
    padding: 5px 15px;
    text-overflow: ellipsis;
    overflow: hidden;
    font-size: 25px;
    font-weight: 500;
    margin-bottom: 15px;
    margin-top: 0;
    color: #68665a;
    text-align:center;
}
.ant-form-item-label label img {
    margin-right: 10px;
    height: 18px;
    display: none;
}
.form-wrapper .ant-form-item-label label {
    color: #444;
}
.form-submit-authentication .form-submit-authentication-button {
    display: flex;
    align-items: center;
    color: #fff;
    height: 50px;
    font-size: 22px;
    background-color: #696857;
    padding: 0 15px;
    border: none;
    border-radius: 5px;
}
@media (max-width: 575px) {
.ant-form-item-control-wrapper, .ant-form-item-label {
    display: inline-block;
}}
.ant-form-item-control {
    position: relative;
    line-height: 30px;
    zoom: 1;
}
.form-builder-wrapper-view {
    height: 100%;
    overflow-x: hidden;
}
.form-forgot-password {
    color: #b5b5b5;
    margin-left: auto;
    padding: 0 17px 30px;
    margin-top: -5px;
    font-size: 13px;
    text-align: right;
    font-weight: 400;
}

Email  Password test123 Forgot Password? You may now proceedI consent to the sharing of my covid 19 Rapid test result with the test manufacturer, supplier, and their associated entities and those individuals and companies I nominate within my profile.

I Consent.  
            
Your DetailsFull Name  Birth Date May2000SuMoTuWeThFrSa301234567891011121314151617181920212223242526272829303112345678910Today Select your Province Select... Your MyCovidRecord Email  Pick an identification document Select... Enter Document Number  Are you Fully Vaccinated against COVID19? Select... Your Photo Click or drag file here to upload 
        
Time to Test!
Click &quot;Add New&quot; to record a new test. Also access your test history here.
Tests Add New Click on Submit once you are done recording your test resultsSubmitPowered by WorkMap.ai' or . = 'DownloadRapid Test Registration
    
New Users.Enter details &amp; remember for future access.

        

Returning Users.Enter your credentials.


.form-view-box-wrapper .builder-form-title, .form-wrapper-box .builder-form-title {
    text-transform: capitalize;
    padding: 5px 15px;
    text-overflow: ellipsis;
    overflow: hidden;
    font-size: 25px;
    font-weight: 500;
    margin-bottom: 15px;
    margin-top: 0;
    color: #68665a;
    text-align:center;
}
.ant-form-item-label label img {
    margin-right: 10px;
    height: 18px;
    display: none;
}
.form-wrapper .ant-form-item-label label {
    color: #444;
}
.form-submit-authentication .form-submit-authentication-button {
    display: flex;
    align-items: center;
    color: #fff;
    height: 50px;
    font-size: 22px;
    background-color: #696857;
    padding: 0 15px;
    border: none;
    border-radius: 5px;
}
@media (max-width: 575px) {
.ant-form-item-control-wrapper, .ant-form-item-label {
    display: inline-block;
}}
.ant-form-item-control {
    position: relative;
    line-height: 30px;
    zoom: 1;
}
.form-builder-wrapper-view {
    height: 100%;
    overflow-x: hidden;
}
.form-forgot-password {
    color: #b5b5b5;
    margin-left: auto;
    padding: 0 17px 30px;
    margin-top: -5px;
    font-size: 13px;
    text-align: right;
    font-weight: 400;
}

Email  Password test123 Forgot Password? You may now proceedI consent to the sharing of my covid 19 Rapid test result with the test manufacturer, supplier, and their associated entities and those individuals and companies I nominate within my profile.

I Consent.  
            
Your DetailsFull Name  Birth Date May2000SuMoTuWeThFrSa301234567891011121314151617181920212223242526272829303112345678910Today Select your Province Select... Your MyCovidRecord Email  Pick an identification document Select... Enter Document Number  Are you Fully Vaccinated against COVID19? Select... Your Photo Click or drag file here to upload 
        
Time to Test!
Click &quot;Add New&quot; to record a new test. Also access your test history here.
Tests Add New Click on Submit once you are done recording your test resultsSubmitPowered by WorkMap.ai')]</value>
      <webElementGuid>e5f1b47a-2f2b-4829-8a5e-b8ad565bf9a9</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
