<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Testing Queue (1)</name>
   <tag></tag>
   <elementGuidId>a219cf4e-1a02-4f67-bab1-fccce1d445f2</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>li.ant-menu-item.ant-menu-item-active > a</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//ul[@id='4$Menu']/li[2]/a</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>80bcd2c3-2226-4a2a-a5df-280a93982608</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>#/boards/7</value>
      <webElementGuid>e6b2cc80-374b-4858-9eff-5af95072339c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Testing Queue</value>
      <webElementGuid>e8604bcf-7274-4c25-a377-eca2bda307c0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;4$Menu&quot;)/li[@class=&quot;ant-menu-item ant-menu-item-active&quot;]/a[1]</value>
      <webElementGuid>831cf2c9-dbf8-41d1-9547-7373671760db</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//ul[@id='4$Menu']/li[2]/a</value>
      <webElementGuid>65888db9-036b-4bf6-995a-b352ac634003</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'Testing Queue')]</value>
      <webElementGuid>99f2c527-de24-4a01-935e-c7be2bbc0cb6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Dashboard'])[1]/following::a[1]</value>
      <webElementGuid>8109808f-202a-41c0-8ed6-8090d1e527e9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='PoC Testing'])[1]/following::a[2]</value>
      <webElementGuid>3fa0589a-aac8-434c-b543-42c4342ec72d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='All Patients'])[1]/preceding::a[1]</value>
      <webElementGuid>3b78ba9c-2714-4906-8f1a-01492f4e36be</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='HP'])[1]/preceding::a[2]</value>
      <webElementGuid>8cc32ab1-e0a4-44f0-9bde-826f2c0e91fa</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Testing Queue']/parent::*</value>
      <webElementGuid>da037cc7-998b-462e-bdfb-4e75f8b30abc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, '#/boards/7')]</value>
      <webElementGuid>c6ffd4af-cca1-4fda-905e-9f62af52f544</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[2]/a</value>
      <webElementGuid>1f12819e-d5c5-4575-b358-86306bbae0b2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = '#/boards/7' and (text() = 'Testing Queue' or . = 'Testing Queue')]</value>
      <webElementGuid>92874bc6-669c-4713-a3a4-b78613540320</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
