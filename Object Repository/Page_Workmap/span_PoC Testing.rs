<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_PoC Testing</name>
   <tag></tag>
   <elementGuidId>9ed9c34c-d33c-41d5-a9d8-62159936a2cf</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//*[@id=&quot;root&quot;]/section/div/div[2]/div/div/div/ul/li[1]/div</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.ant-menu-submenu-title > span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>7dc0ddef-bf13-465a-b281-e1b0859c59a5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>PoC Testing</value>
      <webElementGuid>aecb8ef5-6c54-4299-a8d5-b9678734cb06</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/section[@class=&quot;first-layout ant-layout ant-layout-has-sider&quot;]/div[@class=&quot;drawer-content open&quot;]/div[@class=&quot;rcs-custom-scroll&quot;]/div[@class=&quot;rcs-outer-container&quot;]/div[@class=&quot;rcs-inner-container&quot;]/div[1]/ul[@class=&quot;ant-menu drawer-menu ant-menu-light ant-menu-root ant-menu-inline&quot;]/li[@class=&quot;ant-menu-submenu ant-menu-submenu-inline ant-menu-submenu-active&quot;]/div[@class=&quot;ant-menu-submenu-title&quot;]/span[1]</value>
      <webElementGuid>0abe2a85-6ce5-45be-94f4-c6695c740a4d</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/section/div/div[2]/div/div/div/ul/li/div/span</value>
      <webElementGuid>6b4e3b20-2235-4799-93cb-07ca75e8ee49</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Apps'])[1]/following::span[1]</value>
      <webElementGuid>b255f5c3-a791-498b-96ab-8e4946724d51</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='SA'])[1]/following::span[1]</value>
      <webElementGuid>7844ac3a-829c-4044-98b2-e0c26c8e32b1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Home Testing'])[1]/preceding::span[1]</value>
      <webElementGuid>a32053fc-4835-4a77-a0a0-897b1afaca0a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='New App'])[1]/preceding::span[2]</value>
      <webElementGuid>2091c2ed-a3d9-47ac-9401-bba827165b8a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='PoC Testing']/parent::*</value>
      <webElementGuid>b2b01ae2-0e7e-4467-9811-c8bca35c604b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li/div/span</value>
      <webElementGuid>b0619fbb-8176-419a-bd7c-48e5d1ef7fb3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'PoC Testing' or . = 'PoC Testing')]</value>
      <webElementGuid>98a47d88-0e69-449c-9884-8aa967789eb9</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
