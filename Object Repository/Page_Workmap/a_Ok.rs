<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Ok</name>
   <tag></tag>
   <elementGuidId>fd68bca1-a7c1-4b0d-8d08-e58731d0639b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>a.ant-calendar-ok-btn</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='content']/form/div/div[4]/div[2]/div/span/span/div/div/div/div/div/div/div[2]/div[3]/span/a[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>081400ac-09dc-4670-843b-989f331140f3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>ant-calendar-ok-btn</value>
      <webElementGuid>62ddb1ea-7ff6-465f-b83b-f4b7ba05046e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>role</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>e6c292c4-38cc-42ef-81e1-e76e905da6c5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Ok</value>
      <webElementGuid>2dcdbb4f-32c7-4cde-b2fc-33e7e2fcbd48</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[@class=&quot;app ant-scrolling-effect&quot;]/div[4]/div[@class=&quot;ant-modal-root&quot;]/div[@class=&quot;ant-modal-wrap entry-modal linked-item-details&quot;]/div[@class=&quot;ant-modal&quot;]/div[@class=&quot;ant-modal-content&quot;]/div[@class=&quot;ant-modal-body&quot;]/div[1]/div[@class=&quot;ant-row&quot;]/div[@class=&quot;ant-col ant-col-24 modal-content default-modal&quot;]/div[@id=&quot;content&quot;]/div[@id=&quot;content&quot;]/form[@class=&quot;ant-form ant-form-horizontal&quot;]/div[@class=&quot;ant-row-flex&quot;]/div[@class=&quot;ant-row ant-form-item schedule_input form-view-property&quot;]/div[@class=&quot;ant-col ant-form-item-control-wrapper&quot;]/div[@class=&quot;ant-form-item-control has-success&quot;]/span[@class=&quot;ant-form-item-children&quot;]/span[@class=&quot;datepicker-input ant-calendar-picker&quot;]/div[1]/div[1]/div[1]/div[@class=&quot;ant-calendar-picker-container ignore-inner-clicks ant-calendar-picker-container-placement-topLeft&quot;]/div[@class=&quot;ant-calendar ant-calendar-time&quot;]/div[@class=&quot;ant-calendar-panel&quot;]/div[@class=&quot;ant-calendar-date-panel&quot;]/div[@class=&quot;ant-calendar-footer ant-calendar-footer-show-ok&quot;]/span[@class=&quot;ant-calendar-footer-btn&quot;]/a[@class=&quot;ant-calendar-ok-btn&quot;]</value>
      <webElementGuid>c2ce8bed-df8e-4214-a539-21e949dc9ee2</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='content']/form/div/div[4]/div[2]/div/span/span/div/div/div/div/div/div/div[2]/div[3]/span/a[2]</value>
      <webElementGuid>8ec0948b-c2bc-4d0c-8a32-ac8befe91c9c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'Ok')]</value>
      <webElementGuid>47d2cd08-ef26-488a-bf58-9f72133596ad</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='select time'])[1]/following::a[1]</value>
      <webElementGuid>16d6a41d-c05b-417c-a5ae-a918af098fee</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Sa'])[1]/following::a[2]</value>
      <webElementGuid>4570d9d7-b325-415d-9046-0176cbd4206a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Ok']/parent::*</value>
      <webElementGuid>526d6f2b-2100-4728-a3a0-6999ac140687</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/span/a[2]</value>
      <webElementGuid>1094070e-4288-4509-8253-1acab4f6686f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[(text() = 'Ok' or . = 'Ok')]</value>
      <webElementGuid>f111458f-67a6-4689-ab7a-814fa9fd3e74</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
