<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>li_Negative (1) (2)</name>
   <tag></tag>
   <elementGuidId>2db832af-b070-4565-8e30-e63ba544c1ac</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>li.ant-dropdown-menu-item.available-option.ant-dropdown-menu-item-active</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='content']/div[3]/div/div/ul/li[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>li</value>
      <webElementGuid>26e85a9f-aa4a-46ec-9bf9-263d9b8e6e78</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>ant-dropdown-menu-item available-option ant-dropdown-menu-item-active</value>
      <webElementGuid>8f06f31f-6b5e-45db-afb5-82ff5c571743</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>role</name>
      <type>Main</type>
      <value>menuitem</value>
      <webElementGuid>01c318eb-b882-43dd-aa6a-54490c5fedf9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Negative</value>
      <webElementGuid>503eafff-e089-432f-8727-f368f717ee33</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[@class=&quot;app&quot;]/div[4]/div[@class=&quot;ant-modal-root&quot;]/div[@class=&quot;ant-modal-wrap entry-modal linked-item-details&quot;]/div[@class=&quot;ant-modal&quot;]/div[@class=&quot;ant-modal-content&quot;]/div[@class=&quot;ant-modal-body&quot;]/div[1]/div[@class=&quot;ant-row&quot;]/div[@class=&quot;ant-col ant-col-24 modal-content default-modal&quot;]/div[@id=&quot;content&quot;]/div[@id=&quot;content&quot;]/div[3]/div[1]/div[@class=&quot;ant-dropdown scrollable-dropdown ant-dropdown-placement-bottomLeft&quot;]/ul[@class=&quot;ant-dropdown-menu ant-dropdown-menu-light ant-dropdown-menu-root ant-dropdown-menu-vertical&quot;]/li[@class=&quot;ant-dropdown-menu-item available-option ant-dropdown-menu-item-active&quot;]</value>
      <webElementGuid>fb7fa7c8-3923-4520-9a54-6c69eae78158</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='content']/div[3]/div/div/ul/li[2]</value>
      <webElementGuid>08cc4d63-a352-4730-ba41-1109b602c6e3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Positive'])[2]/following::li[1]</value>
      <webElementGuid>fe0e995a-1a9c-4349-928d-8c7f2d7d1ba0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='.'])[3]/following::li[2]</value>
      <webElementGuid>09e73f67-a2f6-414f-866e-282df9c6b42f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Invalid'])[2]/preceding::li[1]</value>
      <webElementGuid>5e667102-04b1-478c-bc96-253857fcbf49</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//ul/li[2]</value>
      <webElementGuid>8d7e4c5c-9d6f-40ee-916f-9eabf2aad0bd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//li[(text() = 'Negative' or . = 'Negative')]</value>
      <webElementGuid>0373e1ef-11cf-4550-8cff-381f82fcb993</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
