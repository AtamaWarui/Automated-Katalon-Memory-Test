<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Select (3) (1)</name>
   <tag></tag>
   <elementGuidId>6d460b9e-3e99-4a3e-bd98-3e690f138d62</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>span.custom-selection-placeholder</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='content']/form/div/div[15]/div/div[2]/div/span/div/span/span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>abec3681-ad20-4bbf-b35d-21f81277c521</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>custom-selection-placeholder</value>
      <webElementGuid>5beaf6c7-4c56-4a2b-9842-ea63661e3984</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Select...</value>
      <webElementGuid>aeb42be2-6ccb-4d4a-aaad-f72360cca8cb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;content&quot;)/form[@class=&quot;ant-form ant-form-horizontal&quot;]/div[@class=&quot;ant-row-flex&quot;]/div[@class=&quot;ant-col ant-col-xs-24 ant-col-sm-24 ant-col-md-24 ant-col-lg-12 ant-col-xl-12 ant-col-xxl-12&quot;]/div[@class=&quot;ant-row ant-form-item single_option_input form-view-property&quot;]/div[@class=&quot;ant-col ant-form-item-control-wrapper&quot;]/div[@class=&quot;ant-form-item-control&quot;]/span[@class=&quot;ant-form-item-children&quot;]/div[@class=&quot;ant-form-input ant-input custom-select-wrapper&quot;]/span[@class=&quot;placeholder ant-dropdown-trigger custom-select-dropdown&quot;]/span[@class=&quot;custom-selection-placeholder&quot;]</value>
      <webElementGuid>e02020bc-4cd8-40cf-8a07-2b3747ed4b81</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='content']/form/div/div[15]/div/div[2]/div/span/div/span/span</value>
      <webElementGuid>8e9543e4-0ea4-4714-ba24-97b68652fe25</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Select your Province'])[1]/following::span[3]</value>
      <webElementGuid>b4fe574f-467f-4e80-98f9-91cde123c0b8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Birth Date'])[1]/following::span[6]</value>
      <webElementGuid>caa49867-5b2b-4eeb-882b-5abaedefa249</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Your MyCovidRecord Email'])[1]/preceding::span[1]</value>
      <webElementGuid>8b3c7749-05e3-4716-9003-0f23e73c2e80</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Pick an identification document'])[1]/preceding::span[3]</value>
      <webElementGuid>d5b78315-9896-442e-b6de-d62333cad745</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Select...']/parent::*</value>
      <webElementGuid>ef1c0d52-3926-495a-9df6-0e31b40f5d97</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//span/div/span/span</value>
      <webElementGuid>8e3c116c-1554-4dcc-8adb-accd08d55f2f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'Select...' or . = 'Select...')]</value>
      <webElementGuid>21ca4c24-9628-4086-894c-7c9cd33fcdca</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
