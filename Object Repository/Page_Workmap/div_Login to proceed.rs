<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Login to proceed</name>
   <tag></tag>
   <elementGuidId>4a22b6a8-d7ea-4783-bc13-a59d74fd8a90</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.ant-col.ant-col-24.form-text-authentication > div</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='content']/form/div/div[6]/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>61ef8696-716f-4bb3-8f38-066113026703</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Login to proceed</value>
      <webElementGuid>88762573-6003-4be2-a54d-c19fa45bbe0d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;content&quot;)/form[@class=&quot;ant-form ant-form-horizontal&quot;]/div[@class=&quot;ant-row-flex&quot;]/div[@class=&quot;ant-col ant-col-24 form-text-authentication&quot;]/div[1]</value>
      <webElementGuid>1c481c8c-6b68-46bc-aa2f-0bf54ff8bd08</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='content']/form/div/div[6]/div</value>
      <webElementGuid>d578d6ab-dea9-4ef1-a6ab-894a45c53a71</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Forgot Password?'])[1]/following::div[3]</value>
      <webElementGuid>4006f2f1-1083-4e25-9ce3-089b6c71a0ac</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Password'])[1]/following::div[6]</value>
      <webElementGuid>9d7ac87a-87cd-4166-9043-19b26a982e6e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='.'])[1]/preceding::div[4]</value>
      <webElementGuid>894954f3-2a7a-4fe5-a3eb-f1525124db3a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[6]/div</value>
      <webElementGuid>a6fe7a89-f557-48df-87f1-78dd55530015</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = 'Login to proceed' or . = 'Login to proceed')]</value>
      <webElementGuid>804bfcc3-530f-4ef5-ad0c-3ccc35a26ea6</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
