<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>li_Cellife Nasal</name>
   <tag></tag>
   <elementGuidId>94710a87-4e97-43a1-bdd7-db863699a89e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>li.ant-dropdown-menu-item.available-option.ant-dropdown-menu-item-active</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='content']/div[3]/div/div/ul/li[4]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>li</value>
      <webElementGuid>bf461b1e-9480-4a64-9cba-8a7411efa34a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>ant-dropdown-menu-item available-option ant-dropdown-menu-item-active</value>
      <webElementGuid>a73fe16f-d0b4-4473-8b69-c67fe5ce1a8a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>role</name>
      <type>Main</type>
      <value>menuitem</value>
      <webElementGuid>1d714238-5d0c-405b-8aa2-8c2804c2fc99</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Cellife Nasal</value>
      <webElementGuid>e03633ee-1be0-428a-a4a7-5b498c317686</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[@class=&quot;app&quot;]/div[4]/div[@class=&quot;ant-modal-root&quot;]/div[@class=&quot;ant-modal-wrap entry-modal linked-item-details&quot;]/div[@class=&quot;ant-modal&quot;]/div[@class=&quot;ant-modal-content&quot;]/div[@class=&quot;ant-modal-body&quot;]/div[1]/div[@class=&quot;ant-row&quot;]/div[@class=&quot;ant-col ant-col-24 modal-content default-modal&quot;]/div[@id=&quot;content&quot;]/div[@id=&quot;content&quot;]/div[3]/div[1]/div[@class=&quot;ant-dropdown scrollable-dropdown ant-dropdown-placement-bottomLeft&quot;]/ul[@class=&quot;ant-dropdown-menu ant-dropdown-menu-light ant-dropdown-menu-root ant-dropdown-menu-vertical&quot;]/li[@class=&quot;ant-dropdown-menu-item available-option ant-dropdown-menu-item-active&quot;]</value>
      <webElementGuid>145aee42-aec3-486e-b93d-72c7451faa8c</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='content']/div[3]/div/div/ul/li[4]</value>
      <webElementGuid>422d8816-b757-4413-ae8e-ce392b41ec3c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Lolly Saliva'])[2]/following::li[1]</value>
      <webElementGuid>69414bd3-2a4f-4b13-ac12-14160f364b25</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='V-Chek Nasal'])[2]/following::li[2]</value>
      <webElementGuid>fd47f610-1af5-4baa-adc9-8b24bb9a3c68</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[4]</value>
      <webElementGuid>fb757cb0-6581-4f0b-9858-f92778b09d06</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//li[(text() = 'Cellife Nasal' or . = 'Cellife Nasal')]</value>
      <webElementGuid>7d521cb3-bce0-434e-84e7-e76c2f274ee4</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
