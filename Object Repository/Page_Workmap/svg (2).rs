<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>svg (2)</name>
   <tag></tag>
   <elementGuidId>9edf52a7-5834-40ae-87e6-9c3ba5363c56</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>a.main-icon-forward-back.false > svg</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Video Demos'])[1]/following::*[name()='svg'][1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>svg</value>
      <webElementGuid>28956b16-e20a-4420-8d4d-ab2a03e5a7c4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>width</name>
      <type>Main</type>
      <value>20</value>
      <webElementGuid>f44e8c0c-34ad-462f-ad15-23f99ee426fe</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>height</name>
      <type>Main</type>
      <value>20</value>
      <webElementGuid>5ebbbe68-2b76-4c06-9fa6-c58e0ad5f5b0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>viewBox</name>
      <type>Main</type>
      <value>0 0 24 24</value>
      <webElementGuid>5f2e9294-229b-46e8-a716-b8907aebd7b0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;main-content-wrapper&quot;)/a[@class=&quot;main-icon-forward-back false&quot;]/svg[1]</value>
      <webElementGuid>26f2b26d-bd18-473e-958c-e1be9ea8367c</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Video Demos'])[1]/following::*[name()='svg'][1]</value>
      <webElementGuid>1cbbae21-ee1c-43fa-808b-5b2a8810032a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='New App'])[1]/following::*[name()='svg'][1]</value>
      <webElementGuid>8a0c13c8-dff6-4f87-a264-3ad45aaf84f7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Pages'])[1]/preceding::*[name()='svg'][1]</value>
      <webElementGuid>7b20eba3-ef85-4752-bdfb-b4b661b58a50</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Solasta Testing Apps'])[1]/preceding::*[name()='svg'][2]</value>
      <webElementGuid>b88954d2-25f2-4f8e-abae-359b51ea345e</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
