<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_All Patients (2)</name>
   <tag></tag>
   <elementGuidId>d73d2dee-b24a-4e52-a721-7cfbf5522a06</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//ul[@id='4$Menu']/li[3]/a</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>li.ant-menu-item.ant-menu-item-active > a</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>432336ff-a435-4260-bad6-ef23efda00d6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>#/boards/8</value>
      <webElementGuid>a4ac62a6-2f54-4f09-be6f-0992daacd39b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>All Patients</value>
      <webElementGuid>400d479d-3374-4543-9ce5-7afbc1acd07d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;4$Menu&quot;)/li[@class=&quot;ant-menu-item ant-menu-item-active&quot;]/a[1]</value>
      <webElementGuid>d03f8a50-14fa-4eed-a4b6-033acd5c84b8</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//ul[@id='4$Menu']/li[3]/a</value>
      <webElementGuid>b3cc649b-1fc9-41f8-97e8-d0d428997cfe</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'All Patients')]</value>
      <webElementGuid>ca6e279f-3194-4350-9aa2-875a652dae02</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Testing Queue'])[1]/following::a[1]</value>
      <webElementGuid>280d0405-bb98-4b18-9c20-7ef41c0649b6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Dashboard'])[1]/following::a[2]</value>
      <webElementGuid>899a800c-3a0e-4efe-acd0-b3d715fdbcf9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='HP'])[1]/preceding::a[1]</value>
      <webElementGuid>35ceac0a-e861-4756-95dd-0dd155b44efb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='New'])[1]/preceding::a[2]</value>
      <webElementGuid>68c2754f-294d-448e-9bb3-daae13a3a5d3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='All Patients']/parent::*</value>
      <webElementGuid>02784b8d-f991-4f93-821e-4182512fe0c1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, '#/boards/8')]</value>
      <webElementGuid>a0ca6ace-8a5c-4024-8a4d-4e7d2a3250d7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[3]/a</value>
      <webElementGuid>a65128f5-c0c2-4956-b3ea-ee33ab1552aa</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = '#/boards/8' and (text() = 'All Patients' or . = 'All Patients')]</value>
      <webElementGuid>d30ed9de-b4a5-484d-b989-43b547261490</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
