<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>i_Automation_mdi mdi-chart-arc (3)</name>
   <tag></tag>
   <elementGuidId>f3166fa3-9b62-4507-9365-be1520edd342</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//section[@id='main-content-wrapper']/header/div/ul/li[7]/a/i</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>i.mdi.mdi-chart-arc</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>i</value>
      <webElementGuid>c26757d7-52a6-40f5-8970-8bae2cb388f9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>mdi mdi-chart-arc</value>
      <webElementGuid>3e8fdc05-bcab-4f43-8ffd-7c484a4ca344</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;main-content-wrapper&quot;)/header[@class=&quot;ant-layout-header&quot;]/div[@class=&quot;board-page-header&quot;]/ul[1]/li[7]/a[@class=&quot;ant-tooltip-open&quot;]/i[@class=&quot;mdi mdi-chart-arc&quot;]</value>
      <webElementGuid>c6b761f1-f8ed-4e53-99ad-adc16f0da3cc</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//section[@id='main-content-wrapper']/header/div/ul/li[7]/a/i</value>
      <webElementGuid>9f232053-d4f4-4958-b0d6-9598415cf5c9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[7]/a/i</value>
      <webElementGuid>b6647861-758e-4918-b81b-dc03fcaf133b</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
