<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>svg</name>
   <tag></tag>
   <elementGuidId>004fd9d5-0f04-43db-94a3-6dee381f9260</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>a.main-icon-forward-back.false > svg</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Video Demos'])[1]/following::*[name()='svg'][1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>svg</value>
      <webElementGuid>8a0c8810-f970-46ac-9f44-0e4a084d0663</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>width</name>
      <type>Main</type>
      <value>20</value>
      <webElementGuid>8c8606a7-b198-496b-b5f7-3ae3b11243d0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>height</name>
      <type>Main</type>
      <value>20</value>
      <webElementGuid>d8c6b6cf-2f00-4622-ba42-af8236ccf968</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>viewBox</name>
      <type>Main</type>
      <value>0 0 24 24</value>
      <webElementGuid>e413e758-cbff-44a2-a60f-bd0d54efee9e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;main-content-wrapper&quot;)/a[@class=&quot;main-icon-forward-back false&quot;]/svg[1]</value>
      <webElementGuid>f51c6a20-2ca9-4b59-8d4c-10191c1807ac</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Video Demos'])[1]/following::*[name()='svg'][1]</value>
      <webElementGuid>e7d0ad47-1192-4a67-9f30-ec87f03d6ef9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='New App'])[1]/following::*[name()='svg'][1]</value>
      <webElementGuid>c64b4a25-bf98-4176-8461-b5845d704a29</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Brainfood PoC Testing'])[1]/preceding::*[name()='svg'][1]</value>
      <webElementGuid>bef9208e-77a5-487b-8ad0-2e9de41450a3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='PoC Testing Application'])[1]/preceding::*[name()='svg'][2]</value>
      <webElementGuid>2aa1ee8f-330d-4b34-a612-e5346c99929e</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
