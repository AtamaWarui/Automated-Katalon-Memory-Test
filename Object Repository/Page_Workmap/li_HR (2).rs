<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>li_HR (2)</name>
   <tag></tag>
   <elementGuidId>e6b416f9-d889-4054-bbf9-378b330a4007</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//section[@id='main-content-wrapper']/main/div[4]/div/div/ul/li[5]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>li.ant-dropdown-menu-item.report-item.ant-dropdown-menu-item-active</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>li</value>
      <webElementGuid>2a72ab89-f26d-43fa-99f4-b7a7704cdb0b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>ant-dropdown-menu-item report-item ant-dropdown-menu-item-active</value>
      <webElementGuid>0db8329c-cab2-452f-a1cf-9cd21766ea77</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>role</name>
      <type>Main</type>
      <value>menuitem</value>
      <webElementGuid>6d82e353-3926-4462-80ed-3f9546050095</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>HR</value>
      <webElementGuid>5c1673d2-c18c-4b0b-8287-ab634e918262</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;main-content-wrapper&quot;)/main[@class=&quot;ant-layout-content&quot;]/div[4]/div[1]/div[@class=&quot;ant-dropdown reports-list-drop-down ant-dropdown-placement-bottomLeft&quot;]/ul[@class=&quot;ant-dropdown-menu ant-dropdown-menu-light ant-dropdown-menu-root ant-dropdown-menu-vertical&quot;]/li[@class=&quot;ant-dropdown-menu-item report-item ant-dropdown-menu-item-active&quot;]</value>
      <webElementGuid>ff811f4f-0f8a-4383-9676-4564278f9121</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//section[@id='main-content-wrapper']/main/div[4]/div/div/ul/li[5]</value>
      <webElementGuid>710c5151-6205-4b62-b4f5-4fcdac8ba4f9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Production'])[2]/following::li[1]</value>
      <webElementGuid>6170dfbe-1990-498b-a24a-061355f66736</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Marketing'])[1]/following::li[2]</value>
      <webElementGuid>5abf39b9-a448-4063-bf4f-fcd4d98cd814</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div/ul/li[5]</value>
      <webElementGuid>5a503a19-9898-438d-9169-ca15656f35a1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//li[(text() = 'HR' or . = 'HR')]</value>
      <webElementGuid>95e0d3cb-6867-4b2f-984e-a9edf1af2657</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
