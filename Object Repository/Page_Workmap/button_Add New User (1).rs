<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Add New User (1)</name>
   <tag></tag>
   <elementGuidId>33a452bb-55cf-4843-bbdd-d07f81a57666</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>button.ant-btn.save-matching-btn.ant-btn-primary</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//body/div[5]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/button[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>06f2ebad-0c8b-4449-a0a4-a12228dd6e58</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>caf21667-e465-458f-be03-bb7e94cdb778</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>ant-btn save-matching-btn ant-btn-primary</value>
      <webElementGuid>ed6962b8-8ae9-44d7-a09e-30197379f9e4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Add New User</value>
      <webElementGuid>c0105dc5-ae2a-4eaf-82dd-23d3e0dc2329</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;rcDialogTitle1&quot;)/div[@class=&quot;matching-modal-header&quot;]/div[@class=&quot;matching-modal-action&quot;]/button[@class=&quot;ant-btn save-matching-btn ant-btn-primary&quot;]</value>
      <webElementGuid>e19be694-916d-447f-b861-e0b4e8631989</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>(//button[@type='button'])[5]</value>
      <webElementGuid>8e921fcf-40b6-4a2a-9a44-7938075fc14e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='rcDialogTitle1']/div/div/button[2]</value>
      <webElementGuid>405cf23b-4c92-40b5-bfa6-e1243c3162cb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Cancel'])[1]/following::button[1]</value>
      <webElementGuid>88806bdb-6ed6-4a31-8f01-6ffe0f28465e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Owner'])[2]/following::button[2]</value>
      <webElementGuid>c805baef-9850-46e9-ba36-18823b308665</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Account Role'])[1]/preceding::button[1]</value>
      <webElementGuid>f058a7f5-7de4-427c-ad42-8a6d93262105</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//button[2]</value>
      <webElementGuid>4a072804-773a-41dc-bd6c-31468b5bbf2d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[@type = 'button' and (text() = 'Add New User' or . = 'Add New User')]</value>
      <webElementGuid>5abd2661-cddf-4b24-8786-66880eece9c1</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
